angular.module('brighter-ng-test', [])
.directive("brighterMoney", ["$filter", function($filter) {
        // An input field attribute directive that keeps inputs like "$1,512.99" bound to a model like "1512.99" with support for min/max validation and a required flag
        return {
            restrict: 'A',
            require: '?ngModel',
            link: function ($scope, element, attr, controller) {
              
                var min = attr.min || -Infinity;
                var max = attr.max || Infinity;
                var required = attr.required || false;

                controller.$parsers.push(function(text){
                    var transformedInput = text.replace(/[^0-9,\.]+/g, '');
                    var decimalPos = transformedInput.indexOf(".");
                    if (decimalPos != transformedInput.lastIndexOf(".")) {
                        transformedInput = transformedInput.substr(0, transformedInput.lastIndexOf("."));
                    }
                    // Disallow more than 2 decimals
                    if (decimalPos != -1 && transformedInput.substr(decimalPos+1).length > 2) {
                        transformedInput = transformedInput.substr(0, transformedInput.length - 1);
                    }
                    // Set as valid
                    if (transformedInput === "" && !required) {
                        controller.$setValidity("brighter-money", true);
                    } else {
                        var validFormat = /^\$?(\d{1,3},?(\d{3},?)*\d{3}(\.\d{1,3})?|\d{1,3}(\.\d{2})?)$/.test(transformedInput);

                        var val = parseFloat(transformedInput.replace(/[^0-9\.]+/g, ''));
                        var validRange = min <= val && val <= max;
                        controller.$setValidity("brighter-money", validFormat && validRange);
                    }

                    transformedInput = "$" + transformedInput;
                    if(transformedInput !== text) {
                        controller.$setViewValue(transformedInput);
                        controller.$render();
                    }
                    var modelValue = parseFloat(transformedInput.replace(/[^0-9\.]+/g, ''))*100/100;  // strip all formatting for the actual value
                    return isNaN(modelValue) ? null : modelValue
                });

                controller.$formatters.push(function(modelValue){
                    if ((modelValue || '') === '') {
                        return '';
                    }
                    return $filter('currency')(parseFloat(modelValue), "$", 2);
                });
            }
        }
    }]);
